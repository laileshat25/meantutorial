var express = require('express');
var app = express();
var cors = require('cors');
var port = process.env.PORT || 8080;

var morgan = require('morgan');
var mongoose = require('mongoose');
var router = express.Router();
var appRoutes = require('./apps/routes/api')(router);
var path = require('path');
app.use(cors());
app.use(morgan('dev'));
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(express.static(__dirname + '/public'));
app.use('/api', appRoutes);





mongoose.connect('mongodb://localhost:27017/tutorial', function(err, data) {
    if(err) { 
        console.log('Not connected!!!');
    }
    else {
        console.log('Connected successfully !!!');
    }
});


app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/apps/views/index.html'));
});


app.listen(port, function() {

    console.log(`running in server ${port}`);
});