var mongoose = require('mongoose');
  var Schema = mongoose.Schema;

  var bcrypt = require('bcrypt-nodejs');

  var UserSchema = new Schema({

    username: {type: String, lowercase: true, required: true, unique: true},
    email: {type: String, required: true, lowercase: true},
    password: {type: String, required: true}
  });

  UserSchema.pre('save', function(next) {
    var thisuserSchema = this;
    bcrypt.hash(thisuserSchema.password, null, null, function(err, hash) {
      if(err) return next(err);
      thisuserSchema.password = hash;
      next();
    });
  });

  UserSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  }
  module.exports = mongoose.model('User', UserSchema);
  