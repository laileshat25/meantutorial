var User = require('../models/user');
var oracledb = require('oracledb');
var dbConfig = require('../../dbconfig.js');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var url = 'mongodb://localhost:27017/tutorial';

var multer = require('multer');

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'C:/LaptopBackup/angular/meantutorial')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now())
  }
});

var upload = multer({storage: storage});

module.exports = function (router) {
    router.post('/users', (req,res) => {

        var user = new User();
        user.username = req.body.username;
        user.email = req.body.email;
        user.password = req.body.password;
        
        if (req.body.username == null || req.body.username == '' || req.body.password == null || req.body.password == '' || req.body.email == null || req.body.email == '')
        {
            res.send('user name or email or password is missing...');
    
        }
        else {
            user.save(function(err) {
                if (err)
                {
                    res.send('User name or email address alreday exist...');
                }
                else {
                    res.send('User created..');
                }
            });
            
        }
    
        
    });
    
    
    //user authenticate

    router.post('/authenticate', function(req, res) {

        User.findOne({ username: req.body.username }).select('username email password').exec(function(err, user) {
            if (err) throw err;

            if (!user)
            {
                res.json({ "success": false, "message": "Could not authenticate a user "});
            }
            else if (user)
            {
                var validPassword = user.comparePassword(req.body.password);
                if (!validPassword)
                {
                    res.json({"success": false, "message": "Could not authenticate the password"});
                }
                else
                {
                    res.json({"success": true, "message": "user authenticated!! "});
                }

            }
        })
      
    });

    

    router.get('/:env/incomingMessage/:id?', function(req, res) {
      
        oracledb.getConnection(
            {
              user          : dbConfig[req.params.env].user, //dbConfig.DIT.user is also right form
              password      : dbConfig[req.params.env].password,
              connectString : dbConfig[req.params.env].connectString
            },
          
           function(err, connection) {
            if (err) {
              console.error(err.message);
              return;
            }
            console.log('Connection was successful!');
            var query = "select * from incoming_message where correlation_id like \'" + req.params.id + "%\' order by create_date desc";
            //CFOTEST_0934434434334388Q5
            connection.execute(query, ((err, incomingRes) => {
              if(err) {
                console.log(err.message);
                connection.close();
              }
              else {
                this.response = incomingRes;
                connection.close();
                res.header('Content-Type' , 'application/json');
                res.status(200).json(this.response);
              
            }
            }));


          });
         
          
      
    });


    router.get('/:env/requestTable/:id?', function(req1, res1) {
      
        oracledb.getConnection(
            {
              user          : dbConfig[req1.params.env].user, //dbConfig.DIT.user is also right form
              password      : dbConfig[req1.params.env].password,
              connectString : dbConfig[req1.params.env].connectString
            },
          
            function(err, connection) {
            if (err) {
              console.error(err.message);
              return;
            }
            console.log('Connection was successful!');
        
        
            var query = "select * from cce_request where doc_number like \'" + req1.params.id + "%\'";
            
            //CFOTEST_0934434434334388Q5
           connection.execute(query, ((err, db) => {
              if(err) {
                console.log(err.message);
                connection.close();
              }
             // console.log(JSON.stringify(res.metaData));
             else{
             this.response1 = db;
              
              console.log(db);

              connection.close();
              
              res1.status(200).json(this.response1);
             }
            })); 
           
          });
          
          
          
      
    });


    router.get('/:env/requestAttribTable/:id?', function(reqAttrib, resAttrib) {
      
        oracledb.getConnection(
            {
              user          : dbConfig[reqAttrib.params.env].user, //dbConfig.DIT.user is also right form
              password      : dbConfig[reqAttrib.params.env].password,
              connectString : dbConfig[reqAttrib.params.env].connectString
            },
          
            function(err, connection) {
            if (err) {
              console.error(err.message);
              return;
            }
            console.log('Connection was successful!');
        
        
            var query = "select * from cce_req_attrib where cce_request_id like \'" + reqAttrib.params.id + "%\'";
            
            //16942
           connection.execute(query, ((err, resAttrib1) => {
              if(err) {
                console.log(err.message);
                connection.close();
              }
              else
              {
                this.responseAttrib1 = resAttrib1;
                connection.close();
                resAttrib.status(200).json(this.responseAttrib1);
              }
             
            }));
          
          });
          
          
          
      
    }); 
    
    
    router.get('/:env/errorTable/:id?', function(req1, res1) {
      
      
      console.log(env1password);
      oracledb.getConnection(
          {
            user          : dbConfig[req1.params.env].user, //dbConfig.DIT.user is also right form
            password      : dbConfig[req1.params.env].password,
            connectString : dbConfig[req1.params.env].connectString
          },
        
          function(err, connection) {
          if (err) {
            console.error(err.message);
            return;
          }
          console.log('Connection was successful!');
      
          console.log(req1.params.env);
          var query = "select * from cce_error where error_code like \'" + req1.params.id + "%\'";
          
          //9003
         connection.execute(query, ((err, db) => {
            if(err) {
              console.log(err.message);
              connection.close();
            }
           // console.log(JSON.stringify(res.metaData));
           else{
           this.response1 = db;
            
            
            connection.close();
            
            res1.status(200).json(this.response1);
           }
          })); 
         
        });
        
        
        
    
  });
  

  router.get('/:env/customQuery/:id?', function(req1, res1) {
      console.log('hi');
      console.log(req1.params.id);
      
    oracledb.getConnection(
        {
          user          : dbConfig[req1.params.env].user, //dbConfig.DIT.user is also right form
          password      : dbConfig[req1.params.env].password,
          connectString : dbConfig[req1.params.env].connectString
        },
      
        function(err, connection) {
        if (err) {
          console.error(err.message);
          return;
        }
        console.log('Connection was successful!');
    
        console.log(req1.params.env);
        //var query = "select * from cce_error where error_code like \'" + req1.params.id + "\'";
        var query = req1.params.id;
        
        //9003
       connection.execute(query, ((err, db) => {
          if(err) {
            console.log(err.message);
            connection.close();
          }
         // console.log(JSON.stringify(res.metaData));
         else{
         this.response1 = db;
          
          
          connection.close();
          
          res1.status(200).json(this.response1);
         }
        })); 
       
      });
      
      
      
  
});


  router.post('/fileUpload', upload.single('file'), (req, res, next) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        insertDocuments(db, 'C:/LaptopBackup/angular/meantutorial/' + req.file.filename, () => {
            db.close();
            res.json({'message': 'File uploaded successfully'});
        });
    });
});


    return router;



}

var insertDocuments = function(db, filePath, callback) {
  var collection = db.collection('fileupload');
  collection.insertOne({'imagePath' : filePath }, (err, result) => {
      assert.equal(err, null);
      callback(result);
  });
}

