module.exports = {
  "DIT": {
    
    user          : process.env.NODE_ORACLEDB_USER || "cce_owner",
  
    // Instead of hard coding the password, consider prompting for it,
    // passing it in an environment variable via process.env, or using
    // External Authentication.
    password      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
  
    // For information on connection strings see:
    // https://oracle.github.io/node-oracledb/doc/api.html#connectionstrings
    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc3de1dbscn.us.dell.com:1521/ccm2i_sql.dit.amer.dell.com",
  
    // Setting externalAuth is optional.  It defaults to false.  See:
    // https://oracle.github.io/node-oracledb/doc/api.html#extauth
    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
    
  },
  "SIT1": {
    user          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    password      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc3de1dbscn.us.dell.com:1521/ccm1s_default.sit.amer.dell.com",
    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
    
  },
  "SIT2": {
    user          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    password      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc3de1dbscn.us.dell.com:1521/ccm2s_sql.sit.amer.dell.com",
    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
    
  },
  "SIT3": {
    user          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    password      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc6de1dbscn.us.dell.com:1521/ccm3s_sql.sit.amer.dell.com",
    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
    
  },
  "SIT4": {
    user          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    password      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mge12de1dbscn.us.dell.com:1521/ccm4s_default.sit.amer.dell.com",
    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
    
  }



  };
  

  /*userDIT          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    passwordDIT      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectStringDIT : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc3de1dbscn.us.dell.com:1521/ccm2i_sql.dit.amer.dell.com",
    userSIT          : process.env.NODE_ORACLEDB_USER || "cce_owner",
    passwordSIT      : process.env.NODE_ORACLEDB_PASSWORD || "CCE_OWNER$123",
    connectStringSIT : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "mggc3de1dbscn.us.dell.com:1521/ccm2i_sql.dit.amer.dell.com",
    */